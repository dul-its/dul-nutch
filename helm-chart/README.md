# Deploying DUL Nutch with Helm Chart
[[_TOC_]]

## Requirements
1. Helm (*author uses 3.4.1*) - see [installation notes here](https://helm.sh/docs/intro/install/)
2. Openshift CLI (oc)

## Before starting...
Visit our [Openshift environment](https://manage.cloud.duke.edu/console/catalog), then:
1. Locate and click your "NetID" in the upper right portion of the window,
2. Click "Copy Login Command"

## TL;DR
```sh
$ <paste your oc login url here>
$ oc project <your-namespace-is-your-project>
$ cd /path/to/dul-nutch
$ helm delete <your-release>
$ helm install <your-release> helm-chart/
```
## Things To Do
Discover how to `helm install` the project from a GitLab CI/CD process.

