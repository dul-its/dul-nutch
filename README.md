# DUL Nutch

Configurations for [Apache Nutch](http://nutch.apache.org/) (version 1.18) for crawling/indexing DUL website content for search, especially via the [DUL Quicksearch](https://gitlab.oit.duke.edu/dul-its/dul-quicksearch) application.

Binaries must first be downloaded from http://nutch.apache.org/downloads.html -- about 550MB in all. This `dul-nutch` repo includes only the configuration files; the rest are ignored via `.gitignore`

## Requirements
* Apache nutch 1.19 (version 1.20 is available)
* Solr 9

## How Does This Work?
See documentation in DUL Confluence (permitted users only): https://duldev.atlassian.net/wiki/spaces/DSTP/pages/2731606017/Nutch+Website+Search+Overview

## Architecture
### Nutch (proper)
Runs on `nutch-2.lib.duke.edu`

### Solr
Solr instance for Nutch use is running on `nutch-2.lib.duke.edu`

## Continuous Integration/Deployment (CI/CD)

Currently disabled until a new runner is created for `nutch-2.lib.duke.edu`

#### Nutch Pipeline
The Nutch-specific pipeline will run when one (or more) of the following files have been changed:
- 

#### Solr Pipeline
The pipeline dedicated to Solr will run when `schema.xml` changes.

The Solr core for "Nutch" is reloaded after the updated schema is deployed.

## Utility Scripts

`nutch-crawl-stats`  
This Perl script will attempt to read file (provided as argument) and print the stats collected from its 
associated (job) run.  
  
The output will resemble:  
```bash
[dlc32@nutch ~]$ ./nutch-crawl-stats /var/log/nutch_crawl.log-20230918
{"duplicates-deleted":22,"redirects-deleted":1,"add/updated":5761,"gone-deleted":12}

```
  
This output can be available to a Zabbix (task) item and used for graphing purposes.
