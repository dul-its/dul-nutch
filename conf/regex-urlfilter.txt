# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# The default url filter.
# Better for whole-internet crawling.
# Please comment/uncomment rules to your needs.

# Each non-comment, non-blank line contains a regular expression
# prefixed by '+' or '-'.  The first matching pattern in the file
# determines whether a URL is included or ignored.  If no pattern
# matches, the URL is ignored.

# skip file: ftp: and mailto: urls
-^(?:file|ftp|mailto):

# skip URLs longer than 2048 characters, see also db.max.outlink.length
#-^.{2049,}

# skip image and other suffixes we can't yet parse
# for a more extensive coverage use the urlfilter-suffix plugin
# DUL CUSTOMIZATION: also skip assets with parameters e.g., fingerprinted Drupal JS
#-(?i)\.(?:gif|jpg|png|ico|css|sit|eps|wmf|zip|ppt|mpg|xls|gz|rpm|tgz|mov|exe|jpeg|bmp|js)$
-(?i)\.(?:gif|jpg|png|ico|css|sit|eps|wmf|zip|ppt|mpg|xls|gz|rpm|tgz|mov|exe|jpeg|bmp|js)(\?[\w=\-\.]*)?$

# DUL CUSTOMIZATION: instead of skipping ALL URLs with query params, skip them UNLESS it's
# a libguides URL, where tab content pages, subjects, and profiles all use ? in the URL.
#-[?*!@=]
-^((?!https:\/\/guides\.library\.duke\.edu?).)+([?])+

# skip URLs with slash-delimited segment that repeats 3+ times, to break loops
-.*(/[^/]+)/[^/]+\1/[^/]+\1/

# For safe web crawling if crawled content is exposed in a public search interface:
# - exclude private network addresses to avoid that information
#   can be leaked by placing links pointing to web interfaces of services
#   running on the crawling machines (e.g., HDFS, Hadoop YARN)
# - in addition, file:// URLs should be either excluded by a URL filter rule
#   or ignored by not enabling protocol-file
#
# - exclude localhost and loop-back addresses
#     http://localhost:8080
#     http://127.0.0.1/ .. http://127.255.255.255/
#     http://[::1]/
#-^https?://(?:localhost|127(?:\.(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))){3}|\[::1\])(?::\d+)?(?:/|$)
#
# - exclude private IP address spaces
#     10.0.0.0/8
#-^https?://(?:10(?:\.(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))){3})(?::\d+)?(?:/|$)
#     192.168.0.0/16
#-^https?://(?:192\.168(?:\.(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))){2})(?::\d+)?(?:/|$)
#     172.16.0.0/12
#-^https?://(?:172\.(?:1[6789]|2[0-9]|3[01])(?:\.(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))){2})(?::\d+)?(?:/|$)

# accept anything else
# +.

# DUL CUSTOMIZATIONS: ignore these patterns
# =========================================

# Omit all URLs from learninginnovation.duke.edu
-^https?://learninginnovation\.duke\.edu

# Drupal stuff we don't want indexed
-^https?://library.duke.edu/tags
-^https?://library.duke.edu/east
-^https://library.duke.edu/all-jobs-rss-feed
-^https://law.duke.edu/lib/research-guides
-\/feed
-\/aggregator[/]?
-\/node
-\/tags\/
-\/Shibboleth.sso\/

# Wordpress stuff we don't want indexed
-\/wp-admin
-\/wp-json
-\/wp-includes
-\/xmlrpc
-\/tag\/
-\/category\/
-\/author\/
-^https?://blogs.library.duke.edu/blog/duke_featured_posts

# other patterns to ignore
-^https?://guides.library.duke.edu/srch.php

# omit staff dir pages by ID (prefer abc slugs instead)
# i.e., dept or staff slug in URL can't begin with a 0-9 digit
-^https?://library.duke.edu/about/directory/staff/\d+
-^https?://library.duke.edu/about/directory/dept/\d+

# additional file extensions to ignore
-.svg$
-.xml$
-.xlsx$
-.doc$
-.docx$

# misc legacy URLs to omit
-^https?://library.duke.edu/rubenstein/findingaids
-^https?://library.duke.edu/digitalcollections
-^https?://library.duke.edu/about/directory
-^https?://library.duke.edu/apps/directory
-^https?://library.duke.edu/metasearch
-^https?://library.duke.edu/librarycatalog
-^https?://library.duke.edu/rubenstein/scriptorium/
-^https?://library.duke.edu/papyrus/
-^https?://blogs.library.duke.edu/digital-collections

# DUL CUSTOMIZATIONS: define DUL URLs.
# ====================================

# Main DUL website exceptions
# Note: first matching rule determines whether filtered/included
# ---------------------------

# a couple exception library.duke.edu pages that have to end in backslash
# due to a Drupal multisite quirk
+^https://library.duke.edu/data/$
+^https://library.duke.edu/rubenstein/$

# Omit all library.duke.edu URLs ending in backslash
# Note: this is needed because our pages are accessible at both paths with/out slash
# The urlnormalizer-slash plugin (& slashes.txt config) can't be used in this case
# because we have a mix of slash/no-slash canonical pages at host library.duke.edu
-^(?:https:\/\/library\.duke\.edu)(?:.+)[\/]$

# Include all other library.duke.edu URLs
+^https://library.duke.edu


# DUL websites under *.library.duke.edu
# -------------------------------------
+^https://blogs.library.duke.edu
+^https://guides.library.duke.edu
+^https://exhibits.library.duke.edu
+^https://directory.library.duke.edu
+^https://dbtrials.library.duke.edu
+^https://naming.library.duke.edu

# Non-library.duke.edu DUL sites
# ------------------------------
+^https://scholarworks.duke.edu

# Services within the DUL (just discover their homepages, don't crawl further)
# -----------------------------------
+^https://link.duke.edu$
+^https://studentaffairs.duke.edu/dining/venues-and-menus/perk$
+^https://twp.duke.edu/twp-writing-studio$
+^https://oit.duke.edu/what-we-do/services/multimedia-project-studio$

# Professional school libraries
# -----------------------------
+^https://library.divinity.duke.edu
+^https://library.fuqua.duke.edu
+^https://mclibrary.duke.edu
+^https://law.duke.edu/lib
+^https://dukekunshan.edu.cn/en/academics/library

# Additional blogs
# ----------------
+^https://sites.fuqua.duke.edu/fordlibrary
+^https://dukelawref.blogspot.com
